namespace owin-test.owin-test.Services
{
    public interface ISampleReadService
    {
        string ReadData();
    }
}
