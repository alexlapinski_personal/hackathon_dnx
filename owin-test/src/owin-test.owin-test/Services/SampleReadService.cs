namespace owin-test.owin-test.Services
{
    public class SampleReadService : ISampleReadService
    {
        public string ReadData()
        {
            return "Hello World!";
        }
    }
}
