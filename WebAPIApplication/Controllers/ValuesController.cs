namespace WebAPIApplication.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNet.Mvc;
    using Npgsql;
    using Dapper;
    using Models;

    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<DataObject> Get()
        {
            return GetValuesFromDb();
            //return new[] { "1", "2", "3", "4" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public DataObject Get(int id)
        {
            return GetValueFromDb(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
            InsertValueIntoDatabase(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
          //TODO
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
          //TODO
        }

        private IEnumerable<DataObject> GetValuesFromDb() {
            var result = new List<DataObject>();
            
            using (var conn = GetConnection())
            {
                conn.Open();    
                
                const string sql = "SELECT id, name FROM some_table";
                var results = conn.Query(sql);
                
                foreach(var resultItem in results) {
                    result.Add(new DataObject{
                        Id = resultItem.id, 
                        Name = resultItem.name
                    });
                }
            }
            return result;
        }

        private DataObject GetValueFromDb(int id) {
          DataObject result = null;
          using (var conn = GetConnection())
          {
              conn.Open();
              const string sql = "SELECT name FROM some_table where id = @id";
              var queryParams = new DynamicParameters();
              queryParams.Add("@id",id);
              var resultObject = conn.Query(sql, queryParams).FirstOrDefault();
              result = new DataObject{Id = id, Name = resultObject.name};

          }
          return result;
        }

        private bool InsertValueIntoDatabase(string value)
        {
            using(var conn  = GetConnection())
            {
                const string sql = "INSERT INTO some_table(name) VALUES (@name)";
                var result = conn.Execute(sql, new {name = value});
                return result == 1;    
            }
            
            
            return false;
        }

        private NpgsqlConnection GetConnection() {
            return new NpgsqlConnection("Host=localhost;Port=5432;Username=postgres;Password=momoiro72;Database=hackathon");
        }
    }
}
